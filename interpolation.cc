//
// Created by maryhallow on 02.12.15.
//
#ifndef MAIN_INTERPOLATION_CC
#define MAIN_INTERPOLATION_CC
#include "interpolation.h"

double Q(double T, double rho) // neutrino emissivity (redshifted temperature, density)
{
    if(test){
        double Q_0 = 1.e1;
        return Q_0*T*T;
    }
    else{
        return pow(10, gsl_interp2d_eval(Q_spline, grid1_interp, grid2_interp, Q_interp, log10(T), log10(rho), xacc, yacc));
//        return pow(10, interp2d(log10(T), log10(rho), 0, "Q"));
    }
}

double Q_source(double T, double rho, double x, double y, double z, double r, double t_step) // neutrino emissivity (redshifted temperature, density)
{
    if(test){
        double Q_0 = 1.e1;
        return Q_0*T*T;
    }
    else{
        return exp(-pow(x-11e5,2)/2.0/pow(1.0e5,2))*
               exp(-(pow(y,2)+pow(z,2))/2.0/pow(1.0e5,2))*
               1e21;
    }
}

double C(double T, double rho) // heat capacity (redshifted temperature, density)
{
    if(test){
        double C_0 = 1.e12;
        return C_0*T;
    }
    else{
        return pow(10, gsl_interp2d_eval(C_spline, grid1_interp, grid2_interp, C_interp, log10(T), log10(rho), xacc, yacc));
//        return pow(10,interp2d(log10(T), log10(rho), 0, "C"));
    }
}

double kappa(double T, double rho) // thermal conductivity (redshifted temperature, density)
{
    if(test){
        double kappa_0 = 1.e12;
        return kappa_0*T;
    }
    else{
        return pow(10, gsl_interp2d_eval(k_spline, grid1_interp, grid2_interp, k_interp, log10(T), log10(rho), xacc, yacc));
//        return pow(10, interp2d(log10(T), log10(rho), 0, "k"));
    }
}

double rho(double radius) // density (radius)
{
    return pow(10,interp1d(log10(radius),0,"rho"));
}

double rho_deform_grid(double radius) // linear interpolation function "density (radius)". Only used at the start of the simulation for grid transformation.
{

    double f_2 = pow(10,model_rho[len_model-1]);
    double f_1 = pow(10,model_rho[1]);
    double x_2 = pow(10,model_r[len_model-1]);
    double x_1 = pow(10,model_r[1]);

    return f_1 + (f_2 - f_1) * (radius - x_1) / (x_2 - x_1);
}

double r_deform_grid(double rho) // linear interpolation function "radius (density)". Only used at the start of the simulation for grid transformation.
{

    double x_2 = pow(10,model_rho[len_model-1]);
    double x_1 = pow(10,model_rho[1]);
    double f_2 = pow(10,model_r[len_model-1]);
    double f_1 = pow(10,model_r[1]);

    return f_1 + (f_2 - f_1) * (rho - x_1) / (x_2 - x_1);
}

double radius(double rho) // radius (density)
{
    return pow(10,interp1d(log10(rho),0,"r"));
}

double mass(double radius) // mass enclosed within a sphere with a radius r "mass (radius)"
{
    return pow(10,interp1d(log10(radius),0,"mass"));
}

double phi(double radius) // gravitational potential (radius)
{
    return interp1d(log10(radius),0,"phi");
}

double TiTe(double T) // external temperature (internal temperature)
{
    return pow(10,(interp1d(log10(T),0,"TiTe")));
}


double interp_calc2d(double x, double y, double x_1, double x_2, double y_1, double y_2, double f_11, double f_21, double f_12, double f_22)  // 2d interpolation function (main part)
{
    return (f_11 * (x_2 - x) * (y_2 - y) + f_21 * (x - x_1) * (y_2 - y) + f_12 * (y - y_1) * (x_2 - x) + f_22 * (x - x_1) * (y - y_1))
           / ((x_2 - x_1) * (y_2 - y_1));
}

double interp_calc1d(double x, double x_1, double x_2, double f_1, double f_2)  // 1d interpolation function (main part)
{
    return f_1 + (f_2 - f_1) * (x - x_1) / (x_2 - x_1);
}

int interp_pointnumber(std::vector<double> points_data, double point, unsigned int reverse)  // interpolation function that finds the element of the vector points_data closest to the point value
{
    if (reverse == 0) {
        unsigned int i = 0;

        while (points_data[i] - point < 0)
            ++i;
        return i;
    }
    else {
        unsigned int i = 0;

        while (points_data[i] - point > 0)
            ++i;
        return i;
    }
}

double interp1d(double x, double extvalue, std::string mode)  // 1d interpolation function
{
    unsigned  int min_index;

    if (mode == "rho") {
        if (x > model_r[len_model - 1] || x < model_r[1]) {
            return extvalue;
        }
        else {
            min_index = interp_pointnumber(model_r, x, 0);
            return interp_calc1d(x, model_r[min_index - 1], model_r[min_index], model_rho[min_index - 1],
                                 model_rho[min_index]);
        }
    }
    else if (mode == "mass") {
        if (x > model_r[len_model - 1] || x < model_r[1]) {
            return extvalue;
        }
        else {
            min_index = interp_pointnumber(model_r, x, 0);
            return interp_calc1d(x, model_r[min_index - 1], model_r[min_index], model_m[min_index - 1],
                                 model_m[min_index]);
        }
    }
    else if (mode == "phi") {
        if (x > model_r[len_model - 1] || x < model_r[1]) {
            return extvalue;
        }
        else {
            min_index = interp_pointnumber(model_r, x, 0);
            return interp_calc1d(x, model_r[min_index - 1], model_r[min_index], model_phi[min_index - 1],
                                 model_phi[min_index]);
        }
    }
    else if (mode == "TiTe") {
        if (x > model_Ti[len_Ti - 1] || x < model_Ti[0]) {
            return extvalue;
        }
        else {
            min_index = interp_pointnumber(model_Ti, x, 0);
            return interp_calc1d(x, model_Ti[min_index - 1], model_Ti[min_index], model_Te[min_index - 1],
                                 model_Te[min_index]);
        }
    }
    else if (mode == "r") {
        if (x < model_rho[len_model - 1] || x > model_rho[1]) {
            return extvalue;
        }
        else {
            min_index = interp_pointnumber(model_rho, x, 1);
            return interp_calc1d(x, model_rho[min_index], model_rho[min_index + 1], model_r[min_index],
                                 model_r[min_index + 1]);
        }
    }
    else {
        return 0;
    }
}

double interp2d(double x, double y, int extvalue, std::string mode)  // 2d interpolation function
{
    unsigned int min_index_x, min_index_y;
    double f_11, f_22, f_12, f_21;

    if( x > grid1[len_x - 1] || y > grid2[0] || x < grid1[0] || y < grid2[len_y - 1]) {
        return extvalue;
    }
    else {

        min_index_x = interp_pointnumber(grid1, x, 0);
        min_index_y = interp_pointnumber(grid2, y, 1);


        if (mode == "Q") {
            f_11 = Q_values[(min_index_x - 1) + len_x * (min_index_y)];
            f_22 = Q_values[min_index_x + len_x * (min_index_y+1)];
            f_12 = Q_values[(min_index_x - 1) + len_x * (min_index_y+1)];
            f_21 = Q_values[min_index_x + len_x * (min_index_y)];
        }
        else if (mode == "C") {
            f_11 = C_values[(min_index_x - 1) + len_x * (min_index_y)];
            f_22 = C_values[min_index_x + len_x * (min_index_y+1)];
            f_12 = C_values[(min_index_x - 1) + len_x * (min_index_y+1)];
            f_21 = C_values[min_index_x + len_x * (min_index_y)];
        }
        else if (mode == "k") {
            f_11 = k_values[(min_index_x - 1) + len_x * (min_index_y)];
            f_22 = k_values[min_index_x + len_x * (min_index_y+1)];
            f_12 = k_values[(min_index_x - 1) + len_x * (min_index_y+1)];
            f_21 = k_values[min_index_x + len_x * (min_index_y)];
        }
        else {
            return 0;
        }

        return interp_calc2d(x, y, grid1[min_index_x - 1], grid1[min_index_x], grid2[min_index_y - 1], grid2[min_index_y],
                             f_11, f_21, f_12, f_22);
    }
}

#endif
