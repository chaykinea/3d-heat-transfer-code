//
// Created by maryhallow on 06.12.15.
//

#ifndef MAIN_AUXILIARY_FUNCTIONS_H
#define MAIN_AUXILIARY_FUNCTIONS_H

std::vector<double> log_space(double start, double stop, unsigned int number_of_points);

#endif //MAIN_AUXILIARY_FUNCTIONS_H
