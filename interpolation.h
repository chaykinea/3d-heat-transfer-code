//
// Created by maryhallow on 02.12.15.
//

#ifndef MAIN_INTERPOLATION_H
#define MAIN_INTERPOLATION_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include "constants.h"
#include "loaddata.cc"
#include <string>

double C(double T, double rho = 0.);
double kappa(double T, double rho = 0.);
double Q(double T, double rho = 0.);
double TiTe(double T);
double radius(double rho);
double mass(double rho);
double phi(double rho);
double rho_deform_grid(double radius);
double r_deform_grid(double rho);
double rho(double radius);

double interp2d(double x, double y, int extvalue, std::string mode);
double interp1d(double x, double extvalue, std::string mode);
double interp_calc1d(double x, double x_1, double x_2, double f_1, double f_2);
int interp_pointnumber(std::vector<double> points_data, double point, unsigned int reverse);
double interp_calc2d(double x, double y, double x_1, double x_2, double y_1, double y_2, double f_11, double f_21, double f_12, double f_22);

#endif //MAIN_INTERPOLATION_H