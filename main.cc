/*This is a skeleton code file for use with the Finite Element Method for Problems in Physics.
It uses the deal.II FEM library, dealii.org*/

//Include files
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <time.h>
#include "FEM_heatequation.cc"
//#include "source.cc"

using namespace dealii;
void TimeOut(time_t StartTime);
void show_configuration(unsigned int source_trigger);
void open_source_cfg();

std::vector<double> power;
std::vector<double> left_boundary;
std::vector<double> right_boundary;
int number_of_simulations;

int main (){

    open_source_cfg();
    show_configuration(source_trigger);
	time_t StartTime;
	StartTime=time(NULL);

	try{
		deallog.depth_console (0);

        loadfiles();

		if(source_trigger){
            for(unsigned int i=0;i<number_of_simulations;i++){
               //FEM_source<dimension> source(Euler_scheme, left_boundary[i], right_boundary[i], power[i] ,i);
               //source.generate_mesh();
               //source.setup_system();
               //source.solve_trans();
            }
		}
		else{
			FEM<dimension> problemObject(Euler_scheme);
			problemObject.generate_mesh();
			problemObject.setup_system();
			problemObject.solve_trans();
		}


	}
	catch (std::exception &exc){
		std::cerr << std::endl << std::endl
		<< "----------------------------------------------------"
		<< std::endl;
		std::cerr << "Exception on processing: " << std::endl
		<< exc.what() << std::endl
		<< "Aborting!" << std::endl
		<< "----------------------------------------------------"
		<< std::endl;

		return 1;
	}
	catch (...){
		std::cerr << std::endl << std::endl
		<< "----------------------------------------------------"
		<< std::endl;
		std::cerr << "Unknown exception" << std::endl
		<< "Aborting!" << std::endl
		<< "----------------------------------------------------"
		<< std::endl;
		return 1;
	}
	TimeOut(StartTime);
	return 0;

}

void TimeOut(time_t StartTime )
{
	int hour, min, sec;
	double duration;
	time_t CurrentTime;

	CurrentTime=time(NULL);
	duration=difftime(CurrentTime, StartTime);
	hour=(int)( duration/3600. );
	min=(int)( (duration - (double)hour*3600.)/60. );
	sec=(int)( duration - (double)hour*3600. - (double)min*60. );
	printf("Time elapsed is %dh %dm %ds \n", hour, min, sec);
}

void open_source_cfg() {
	std::ifstream cfg("./datafiles/config.dat");
	if (cfg.is_open()) {
		double temp;
		unsigned int k = 0;
		while (cfg >> temp) {
			if (k == 0) {
				left_boundary.push_back(temp);
			}
			else if (k == 1) {
				right_boundary.push_back(temp);
			}
			else if (k == 2) {
				power.push_back(temp);
			}
			k++;
			if (k == 3) {
                k = 0;
			}
		}

        number_of_simulations = power.size();
		cfg.close();
	}
}

void show_configuration(unsigned int source_trigger){

    if(source_trigger){

        std::cout << "Heat source is turned on\n" << std::endl;
        std::cout << "Number of simulations with different sources = "  << number_of_simulations << "\n \n" << std::endl;
        std::cout << "Sources\' properties:\n" << std::endl;
        std::cout << "Number Left Right Power (Left and Right source boundaries in density units)" << std::endl;
        std::cout << "-------------------------" << std::endl;
        for(unsigned int j = 0; j<number_of_simulations;j++){
            std::cout << j << " " << left_boundary[j] << " " << right_boundary[j] << " " << power[j] << std::endl;
        }
        std::cout << "-------------------------" << std::endl;

        for(int j = 10; j>=0;j--) {
            std::cout << "Simulation begins in " << j << " seconds..." << std::endl;
            sleep(1);
        }
    }
	else{
        std::cout << "Heat source is turned off\n" << std::endl;
    }
}