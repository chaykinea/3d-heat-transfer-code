//
// Created by maryhallow on 06.12.15.
//

#ifndef MAIN_CONTROL_PANEL_H
#define MAIN_CONTROL_PANEL_H

unsigned int test = 0;                              // when 1  you are solving an analytical test problem, when 0
                                                    // you are solving the real problem
const unsigned int order    = 1;                    // order of basis functions
const unsigned int quadRule = 2;                    // order of quadrature rule
const int dimension = 3;
double Euler_scheme = 1.0 ;

#define CoolingFile "cooling_curve.dat"

const double t_max        = 2.2e3*yrtosec;          // time when computation stops in sec
const double T_0          = 1.e10;                  // initial temperature of the star
const double T_min        = 2.e5;
const double inner_radius = 169229;                 // lower boundary of the star
const double outer_radius = 1158428;                // upper boundary of the star

const int number_of_cells   = 170;                  // number of cell star will be divided into along r axis
const unsigned int N_output = 300;                  // number of data points in output file containing cooling curves

std::vector<double> time_points = {1.e-6, 0.2e-4, 1.e-3, 1.e-2, 1.e-1, 1.e0, 1.e1, 3.e2, 1.e3, 2.e3, 1.e5, 1.e6};
std::vector<double> time_steps =  {0.1e2, 1.e2  , 1.e3 , 1.e4 , 1.e5 , 1.e6, 1.e7, 1.e8, 1.e10,1.e10, 2.e10, 2.e10}; // different values of time step
                                                                                                          // that are used at times from the array above

const double R = 1.e6;                              // R is only needed for analytical test

// ----------------------------------------------------------------
// ---------------------Source parameters--------------------------
// ----------------------------------------------------------------
#define  source_cfg "datafiles/config.dat"
const unsigned int source_trigger      = 0;
const unsigned int number_of_sources   = 2;
const double turn_on_time              = 1e3;
const double t_source_max              = 2.01e3;                                                                    // in years
const unsigned int error_min           = 1;
std::vector<double> t_source_points    = {1000.01,                 1000.1,                  1001.,                    1005.,                    1010.,                    1050.,                    1100.,                      1500.,                      2000.,                      1.e4};   // in years
std::vector<double> t_source_steps     = {time_steps[error_min]*10,time_steps[error_min]*40,time_steps[error_min]*80,time_steps[error_min]*200,time_steps[error_min]*400,time_steps[error_min]*800,time_steps[error_min]*2e3 , time_steps[error_min]*4e3 , time_steps[error_min]*4e3 , time_steps[error_min]*4e3};
// ----------------------------------------------------------------

#endif //MAIN_CONTROL_PANEL_H
