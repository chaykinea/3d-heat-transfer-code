//
// Created by maryhallow on 16.02.16.
//

#ifndef MAIN_SOURCE_H
#define MAIN_SOURCE_H


//Data structures and solvers
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
//Mesh related classes
#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_in.h>
//Finite element implementation classes
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_q.h>

#include <deal.II/grid/manifold_lib.h>
#include "interpolation.cc"
#include "control_panel.h"
#include "auxiliary_functions.cc"

using namespace dealii;

#define r(i) sqrt(pow(nodeLocation[i][0],2)+pow(nodeLocation[i][1],2)+pow(nodeLocation[i][2],2))
const unsigned int manifold = 1;
const unsigned int global_refinement = 3;

template <int dim>
class FEM_source
{
public:

    FEM_source (double Alpha, double Left, double Right, double Power, unsigned int Number_of_source);
    ~FEM_source();

    void generate_mesh();
    void setup_system();
    void assemble_system();
    void apply_initial_conditions();
    void solve_trans();
    void output_profiles_source_dat(unsigned int index);
    void output_profiles_source_vtk(unsigned int index);
    void output_cooling_curve(double time, double T_i, double T_e, unsigned int index);
    double time_step_corrector(double dt,double t,double time_point, unsigned int* error);

    std::vector<double> snapshot = log_space(10, turn_on_time*yrtosec, N_output);
    std::vector<double> snapshot_1 = log_space(turn_on_time*yrtosec, (turn_on_time+10)*yrtosec, N_output*2);
    std::vector<double> snapshot_2 = log_space((turn_on_time+10)*yrtosec, t_source_max*yrtosec, N_output*2);

    Triangulation<dim> triangulation;                         // mesh
    FESystem<dim>      fe;                                    // FE element
    DoFHandler<dim>    dof_handler;                           // Connectivity matrices

    QGauss<dim>   quadrature_formula;                         // Quadrature
    QGauss<dim-1> face_quadrature_formula;                    // Face Quadrature

    SparsityPattern      sparsity_pattern;                    // Sparse matrix pattern
    SparseMatrix<double> M, K, system_matrix;                 // Global stiffness matrix - Sparse matrix - used in the solver
    Vector<double>       D_trans, V_trans, F, RHS;            // Global vectors - Solution vector (D) and Global force vector (F)
    std::map<unsigned int,double> boundary_values_of_D;       // Map of dirichlet boundary conditions for the time derivative of temperature


    Table<2,double>	nodeLocation;	                          // Table of the coordinates of nodes by global dof number

    double alpha; 	                                          // Specifies the Euler method, 0 <= alpha <= 1
    double t_step = 0;                                        // Initial time

    double Left_boundary, Right_boundary, S;
    unsigned int N_source;

    double redshift= sqrt(1 - (2*G*mass(outer_radius))/(outer_radius*c*c));

    //solution name array
    std::vector<std::string> nodal_solution_names;
    std::vector<DataComponentInterpretation::DataComponentInterpretation> nodal_data_component_interpretation;
};

struct grid_transform                                         // grid deform function (this one stands for quater hyper shell )
{
    Point<3> operator() (const Point<3> &in) const
    {
        double temp_r = sqrt(pow(in(0),2)+pow(in(1),2)+pow(in(2),2));

        double theta = acos(in(2)/(temp_r));
        double phi = atan(in(1)/(in(0)));

        double dr_lin = (outer_radius - inner_radius)/pow(2,global_refinement);
        double dr_log = (log10(outer_radius) - log10(inner_radius))/pow(2,global_refinement);

        double n = (temp_r-inner_radius)/dr_lin;

        double r_new = pow(10,log10(inner_radius)+n*dr_log);

        return Point<3> (r_new*sin(theta)*cos(phi), r_new*sin(theta)*sin(phi),r_new*cos(theta));
    }
};

struct grid_transform_2                                       // grid deform function (this one stands for hyperrectangle) (currently used)
{
    Point<3> operator() (const Point<3> &in) const
    {
        double temp_r = in(0);

        double dr_lin = (outer_radius - inner_radius)/number_of_cells;
        double drho_log = (-(log10(rho_deform_grid(outer_radius)) - log10(rho_deform_grid(inner_radius))))/number_of_cells;
        double n = std::floor((temp_r-inner_radius)/dr_lin + 0.5);
        double x_new = r_deform_grid(pow(10,log10(rho_deform_grid(inner_radius))-n*drho_log));

        double y_new = in(1)/outer_radius*x_new;
        double z_new = in(2)/outer_radius*x_new;

        return Point<3> (x_new, y_new, z_new);

    }
};



#endif //MAIN_SOURCE_H
