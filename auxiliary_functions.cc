//
// Created by maryhallow on 06.12.15.
//
#ifndef MAIN_AUXILIARY_FUNCTIONS_CC
#define MAIN_AUXILIARY_FUNCTIONS_CC
#include "auxiliary_functions.h"

std::vector<double> log_space(double start, double stop, unsigned int number_of_points){   // log space function

    std::vector<double> vector(number_of_points);
    double log_start = log10(start);
    double log_stop = log10(stop);
    double log_step = (log_stop - log_start)/(number_of_points-1);

    for (unsigned  i=0; i<number_of_points; i++) {
        vector[i] = pow(10., log_start + i * log_step);
    }

    return vector;
}

#endif