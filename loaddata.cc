//
// Created by maryhallow on 29.11.15.
//
#ifndef MAIN_LOADDATA_CC
#define MAIN_LOADDATA_CC
#include "loaddata.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_interp2d.h>
#include <gsl/gsl_spline2d.h>

// this group of four vectors contains parameters and its mutual dependence on each other for a specific model of the neutron star
// e.g. we know how density depends on radius and vice versa.
std::vector<double> model_r;                                                  // This vector contains radius values (radius = distance form the center of the star)
std::vector<double> model_rho;                                                // This vector contains density values
std::vector<double> model_m;                                                  // This vector contains mass values that is enclosed within a sphere with a radius r if we write m(r)
std::vector<double> model_phi;                                                // This vector contains gravitational potential values
unsigned int len_model;                                                       // The length of each vector out of four above

std::vector<double> model_Ti;                                                 // This vector contains external temperature values
std::vector<double> model_Te;                                                 // This vector contains internal temperature values
unsigned int len_Ti;                                                          // The length of each vector out of two above

std::vector<double> grid1;                                                    // This vector contains redshifted temperature values
unsigned int len_x;                                                           // The length of grid1 vector

std::vector<double> grid2;                                                    // This vector contains density values
unsigned int len_y;                                                           // The length of grid2 vector

// All three matrices below have a size [len_x X len_y]
std::vector<double> Q_values;                                                 // This matrix contains redshifted neutrino emissivity values
std::vector<double> C_values;                                                 // This matrix contains specific heat capacity per unit volume values
std::vector<double> k_values;                                                 // This matrix contains thermal conductivity values

const gsl_interp2d_type *interpolation_type = gsl_interp2d_bilinear;
const size_t nx_interp = 150; /* x grid points along Temperature*/
const size_t ny_interp = 355; /* y grid points along Density*/
double *grid1_interp = (double *) malloc(nx_interp * sizeof(double));
double *grid2_interp = (double *) malloc(ny_interp * sizeof(double));
double *Q_interp = (double *) malloc(nx_interp * ny_interp * sizeof(double));
double *C_interp = (double *) malloc(nx_interp * ny_interp * sizeof(double));
double *k_interp = (double *) malloc(nx_interp * ny_interp * sizeof(double));
gsl_interp2d *Q_spline = gsl_interp2d_alloc(interpolation_type, nx_interp, ny_interp);
gsl_interp2d *C_spline = gsl_interp2d_alloc(interpolation_type, nx_interp, ny_interp);
gsl_interp2d *k_spline = gsl_interp2d_alloc(interpolation_type, nx_interp, ny_interp);
gsl_interp_accel *xacc = gsl_interp_accel_alloc();
gsl_interp_accel *yacc = gsl_interp_accel_alloc();

void loadfiles(void)
{
    int i, j;

    std::ifstream inputFile1("./datafiles/file1.dat");
    if (inputFile1.is_open()) {
        double temp;
        i = 0;

        while (inputFile1 >> temp) {
            grid1.push_back(log10(temp));
            grid1_interp[i] = log10(temp);
            i++;
        }
        inputFile1.close();
    }
    else {
        printf("Temperature file is empty! Simulation is terminated.");
        exit(0);
    }
    len_x = grid1.size();


    std::ifstream inputFile2("./datafiles/file2_rev.dat");
    if (inputFile2.is_open()) {
        double temp;
        i = 0;

        while (inputFile2 >> temp) {
            grid2.push_back(log10(temp));
            grid2_interp[i] = log10(temp);
            i++;
        }
        inputFile2.close();
    }
    else {
        printf("Density file is empty! Simulation is terminated.");
        exit(0);
    }
    len_y = grid2.size();


    std::ifstream inputFile3("./datafiles/file3_rev.dat");
    if (inputFile3.is_open()) {
        double temp;
        i = 0;

        while(inputFile3 >> temp){
            Q_values.push_back(log10(temp));
            Q_interp[i] = log10(temp);
            i++;
        }
        inputFile3.close();
        gsl_interp2d_init(Q_spline, grid1_interp, grid2_interp, Q_interp, nx_interp, ny_interp);
    }
    else {
        printf("Qfile is empty! Simulation is terminated.");
        exit(0);
    }

    std::ifstream inputFile4("./datafiles/file4_rev.dat");
    if (inputFile4.is_open()) {
        double temp;
        i = 0;

        while(inputFile4 >> temp){
            C_values.push_back(log10(temp));
            C_interp[i] = log10(temp);
            i++;
        }
        inputFile4.close();
        gsl_interp2d_init(C_spline, grid1_interp, grid2_interp, C_interp, nx_interp, ny_interp);
    }
    else {
        printf("Cfile is empty! Simulation is terminated.");
        exit(0);
    }


    std::ifstream inputFile5("./datafiles/file5_rev.dat");
    if (inputFile5.is_open()) {
        double temp;
        i = 0;

        while (inputFile5 >> temp){
            k_values.push_back(log10(temp));
            k_interp[i] = log10(temp);
            i++;
        }
        inputFile5.close();
        gsl_interp2d_init(k_spline, grid1_interp, grid2_interp, k_interp, nx_interp, ny_interp);
    }
    else {
        printf("kfile is empty! Simulation is terminated.");
        exit(0);
    }


    std::ifstream inputFile6("./datafiles/tite.dat");
    if (inputFile6.is_open()) {
        double temp;
        unsigned int swtch = 0;
        while (inputFile6 >> temp) {
            if(swtch){
                model_Te.push_back(temp);
                swtch = 0;
            }
            else{
                model_Ti.push_back(temp);
                swtch = 1;
            }
        }
        inputFile6.close();
    }
    else {
        printf("TiTefile is empty! Simulation is terminated.");
        exit(0);
    }
    len_Ti = model_Ti.size();


    std::ifstream inputFile7("./datafiles/mod_dh_37.dat");
    if (inputFile7.is_open()) {
        double temp;
        inputFile7 >> temp;
        inputFile7 >> temp;
        unsigned int swtch = 0;
        while (inputFile7 >> temp) {
            if(swtch==0){
                model_m.push_back(log10(temp*MSun));
                swtch ++;
            }
            else if(swtch==1){
                model_r.push_back(log10(temp* 1.e5));
                swtch ++;
            }
            else if(swtch==3){
                model_rho.push_back(log10(temp));
                swtch ++;
            }
            else if(swtch==4){
                model_phi.push_back(temp);
                swtch ++;
            }
            else if(swtch==7){
                swtch=0;
            }
            else{
                swtch ++;
            }
        }
        inputFile7.close();
    }
    else {
        printf("Modelfile is empty! Simulation is terminated.");
        exit(0);
    }
    len_model = model_rho.size();
}

#endif