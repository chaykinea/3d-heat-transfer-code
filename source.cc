//
// Created by maryhallow on 16.02.16.
//

#include "source.h"


// Class constructor for a scalar field
template <int dim>
FEM_source<dim>::FEM_source (double Alpha, double Left, double Right, double Power, unsigned int Number_of_source)
        :
        fe (FE_Q<dim>(order), 1),                                             // 1 means that we work with a scalar field (temperature)
        dof_handler (triangulation),
        quadrature_formula(quadRule),
        face_quadrature_formula(quadRule)
{
    alpha = Alpha;
    Left_boundary = Left;
    Right_boundary = Right;
    S = Power;
    N_source = Number_of_source;

    nodal_solution_names.push_back("D");
    nodal_data_component_interpretation.push_back(DataComponentInterpretation::component_is_scalar);
}

//Class destructor
template <int dim>
FEM_source<dim>::~FEM_source (){dof_handler.clear ();}

//Define the problem domain and generate the mesh
template <int dim>
void FEM_source<dim>::generate_mesh(){

    if(manifold==0){                                                          // when 0 we are solving our equation on quater hyper shell manifold

        double  x_center = 0.,
                y_center = 0.,
                z_center = 0.;

        Point<dim,double> center(x_center,y_center,z_center);
        GridGenerator::quarter_hyper_shell (triangulation, center, inner_radius, outer_radius, 0, true);
        static const SphericalManifold<3> manifold_description(center);
        triangulation.set_all_manifold_ids(0);
        triangulation.set_manifold (0, manifold_description);
        triangulation.refine_global(global_refinement);
        GridTools::transform(grid_transform(), triangulation);
    }
    else{
        // (currently used)
        std::vector<unsigned int> num_of_elems(dim);
        num_of_elems[0] = number_of_cells;
        num_of_elems[1] = 1;
        num_of_elems[2] = 1;

        double x_min = inner_radius,
                x_max = outer_radius,
                y_min = -1.e4,
                y_max = 1.e4,
                z_min = -1.e4,
                z_max = 1.e4;

        Point<dim,double> min(x_min,y_min,z_min),
                max(x_max,y_max,z_max);
        GridGenerator::subdivided_hyper_rectangle (triangulation, num_of_elems, min, max);
        GridTools::transform(grid_transform_2(), triangulation);


    }
}

// Setup data structures (sparse matrix, vectors)
template <int dim>
void FEM_source<dim>::setup_system(){

    // Let deal.II organize degrees of freedom
    dof_handler.distribute_dofs (fe);

    MappingQ1<dim,dim> mapping;
    std::vector< Point<dim,double> > dof_coords(dof_handler.n_dofs());
    nodeLocation.reinit(dof_handler.n_dofs(),dim);
    DoFTools::map_dofs_to_support_points<dim,dim>(mapping,dof_handler,dof_coords);
    for(unsigned int i=0; i<dof_coords.size(); i++){
        for(unsigned int j=0; j<dim; j++){
            nodeLocation[i][j] = dof_coords[i][j];
        }
    }

    // We Define the size of the global matrices and vectors
    sparsity_pattern.reinit (dof_handler.n_dofs(),
                             dof_handler.n_dofs(),
                             dof_handler.max_couplings_between_dofs());
    DoFTools::make_sparsity_pattern (dof_handler, sparsity_pattern);
    sparsity_pattern.compress();
    K.reinit (sparsity_pattern);
    M.reinit (sparsity_pattern);
    system_matrix.reinit (sparsity_pattern);
    D_trans.reinit(dof_handler.n_dofs());
    V_trans.reinit(dof_handler.n_dofs());
    RHS.reinit(dof_handler.n_dofs());
    F.reinit(dof_handler.n_dofs());

    std::cout << "   Number of active elems:       " << triangulation.n_active_cells() << std::endl;
    std::cout << "   Number of degrees of freedom: " << dof_handler.n_dofs() << std::endl;
}


template <int dim>
void FEM_source<dim>::assemble_system(){

    M=0; K=0; F=0;

    FEValues<dim> fe_values(fe,
                            quadrature_formula,
                            update_values |
                            update_gradients |
                            update_quadrature_points |
                            update_JxW_values);

    FEFaceValues<dim> fe_face_values (fe,
                                      face_quadrature_formula,
                                      update_values |
                                      update_quadrature_points |
                                      update_JxW_values);

    const unsigned int num_face_quad_pts = face_quadrature_formula.size();    // Total number of quad points in the face
    const unsigned int faces_per_elem = GeometryInfo<dim>::faces_per_cell;    // Faces per cell (for our geometry it is 6)

    const unsigned int dofs_per_elem = fe.dofs_per_cell;                      // This gives you dofs per element
    unsigned int 	   num_quad_pts = quadrature_formula.size();              // Total number of quad points in the element

    FullMatrix<double> Mlocal (dofs_per_elem, dofs_per_elem);                 // Mass matrix
    FullMatrix<double> Klocal (dofs_per_elem, dofs_per_elem);                 // Stiffness matrix
    Vector<double>     Flocal (dofs_per_elem);                                // Force vector

    std::vector<unsigned int> local_dof_indices (dofs_per_elem);              // This relates local dof numbering to global dof numbering

    typename DoFHandler<dim>::active_cell_iterator elem = dof_handler.begin_active (), endc = dof_handler.end();
    for (;elem!=endc; ++elem){

        fe_values.reinit(elem);
        elem->get_dof_indices (local_dof_indices);

        Mlocal = 0.;
        Klocal = 0.;
        Flocal = 0.;

        for(unsigned int q=0; q<num_quad_pts; q++){

            double D_trans_at_q = 0.;

            for(unsigned int C=0; C<dofs_per_elem; C++){
                D_trans_at_q += D_trans[local_dof_indices[C]]*fe_values.shape_value(C,q);
            }

            double r_q   = sqrt(pow(fe_values.quadrature_point(q)[0],2) + pow(fe_values.quadrature_point(q)[1],2) + pow(fe_values.quadrature_point(q)[2],2));
            double rho_q = rho(r_q);
            double phi_q = phi(r_q);
            double m_q   = mass(r_q);
            double sqrt_q = sqrt(1 - (2*G*m_q)/(r_q*c*c));
            double exp_q = exp(phi_q);

            double kappa_precalc = kappa(D_trans_at_q,rho_q);
            double C_precalc = C(D_trans_at_q,rho_q);

            for(unsigned int A=0; A<fe.dofs_per_cell; A++){
                if ( (rho_q <= Left_boundary) && (rho_q >= Right_boundary) && t_step/yrtosec >= turn_on_time){
                    Flocal[A] -= fe_values.shape_value(A,q)*(Q(D_trans_at_q,rho_q)-S)/sqrt_q*fe_values.JxW(q);
                }
                else{
                    Flocal[A] -= fe_values.shape_value(A,q)*Q(D_trans_at_q,rho_q)/sqrt_q*fe_values.JxW(q);
                }

                for(unsigned int B=0; B<fe.dofs_per_cell; B++){
                    Mlocal[A][B] += fe_values.shape_value(A,q)*fe_values.shape_value(B,q)*C_precalc/sqrt_q*fe_values.JxW(q);
                    for(unsigned int i=0; i<dim; i++){
                        for(unsigned int j=0; j<dim; j++){
                            if(i==j){
                                Klocal[A][B] += fe_values.shape_grad(A,q)[i]*kappa_precalc*sqrt_q*exp_q*fe_values.shape_grad(B,q)[j]*fe_values.JxW(q);
                            }
                        }
                    }
                }
            }
        }


        for (unsigned int f=0; f < faces_per_elem; f++){
            fe_face_values.reinit (elem, f);
            if(elem->face(f)->center()[0]==outer_radius){

                for (unsigned int q=0; q<num_face_quad_pts; ++q){
                    double T_face = 0.;
                    for(unsigned int C=0; C<dofs_per_elem; C++){
                        T_face += D_trans[local_dof_indices[C]]*fe_face_values.shape_value(C,q);
                    }
                    for (unsigned int A=0; A<dofs_per_elem; A++){
                        Flocal[A] -= sigma*pow(TiTe(T_face*exp(-phi(outer_radius))),4)*exp(2*phi(outer_radius))*fe_face_values.shape_value(A,q)*fe_face_values.JxW(q);
                    }
                }
            }
        }


        for (unsigned int i=0; i<dofs_per_elem; ++i){
            F[local_dof_indices[i]] += Flocal[i];
            for (unsigned int j=0; j<dofs_per_elem; ++j){
                K.add(local_dof_indices[i],local_dof_indices[j],Klocal[i][j]);
                M.add(local_dof_indices[i],local_dof_indices[j],Mlocal[i][j]);
            }
        }
    }
}


// Applying initial conditions
template <int dim>
void FEM_source<dim>::apply_initial_conditions(){

    const unsigned int totalNodes = dof_handler.n_dofs();
    for(unsigned int i=0; i<totalNodes; i++){
            D_trans[i] = T_0;
    }
}


template <int dim>
void FEM_source<dim>::solve_trans(){

    snapshot.resize(snapshot.size()-1);
    snapshot.insert(snapshot.end(),snapshot_1.begin(),snapshot_1.end());
    snapshot.resize(snapshot.size()-1);
    snapshot.insert(snapshot.end(),snapshot_2.begin(),snapshot_2.end());

    int snapshot_length = snapshot.size();

    double delta_t = 10;
    const unsigned int totalNodes = dof_handler.n_dofs();
    Vector<double>     D_tilde(totalNodes);

    apply_initial_conditions();

    unsigned int time_counter = 0;
    unsigned int snap_shot_counter = 0;
    unsigned int time_counter_source = 0;

    while(t_step < t_max){

        t_step += delta_t;

        if(time_counter > error_min) {
            if(t_step>(turn_on_time-100)*yrtosec && t_step <= turn_on_time*yrtosec) {
                delta_t = time_step_corrector(delta_t,t_step,turn_on_time, &time_counter);
            }
        }

        assemble_system();

        D_tilde = D_trans;
        D_tilde.add(delta_t*(1-alpha),V_trans);
        system_matrix.copy_from(M);
        system_matrix.add(alpha*delta_t,K);

        M.vmult(RHS,D_tilde);
        RHS.add(alpha*delta_t,F);

        SparseDirectUMFPACK  A;
        A.initialize(system_matrix);
        A.vmult (D_trans, RHS);

        V_trans = D_trans;
        V_trans.add(-1.,D_tilde);
        V_trans /= alpha*delta_t;

        if (D_trans[0]<T_min || t_step > t_source_max*yrtosec ){
            std::cout << "Simulation has been stopped.\n" << std::endl;
            break;
        }


        if(t_step > snapshot[snap_shot_counter]){
            snap_shot_counter ++;
            std::cout << "time = " << t_step/yrtosec << " years" << std::endl;
            std::cout << "time step = " << delta_t << " sec" << std::endl;
            std::cout << "number of step = " << snap_shot_counter << " out of " << snapshot_length << std::endl;

            for(unsigned int globalNode=0; globalNode<totalNodes; globalNode++) {
                if (nodeLocation[globalNode][0] == outer_radius) {
                    output_cooling_curve( t_step/yrtosec, D_trans[globalNode]*exp(-phi(outer_radius)), TiTe(D_trans[globalNode]*exp(-phi(outer_radius)))*redshift,N_source);
                    break;
                }
            }
        }

        if (t_step < (turn_on_time-100)*yrtosec){
            if(time_counter<time_points.size()){
                if(t_step/yrtosec > time_points[time_counter]){
                    delta_t = time_steps[time_counter];
                    time_counter ++;
                }
            }
        }
        else {
            if (time_counter_source<t_source_points.size()){
                if (t_step/yrtosec > t_source_points[time_counter_source]) {
                    delta_t = t_source_steps[time_counter_source];
                    if(time_counter_source==8) {
                        output_profiles_source_vtk(N_source);
                        output_profiles_source_dat(N_source);
                        std::cout << "current time = " << t_step/yrtosec << "\n" << std::endl;
                    }
                    time_counter_source += 1;
                }
            }
        }
    }
}

template <int dim>
void FEM_source<dim>::output_profiles_source_vtk(unsigned int index){


    char filename[100];
    snprintf(filename, 100, "./output/file_%d.vtk", index);
    std::ofstream output1 (filename);
    DataOut<dim> data_out; data_out.attach_dof_handler (dof_handler);

    data_out.add_data_vector (D_trans, nodal_solution_names, DataOut<dim>::type_dof_data, nodal_data_component_interpretation);
    data_out.build_patches (); data_out.write_vtk (output1); output1.close();
}


template <int dim>
void FEM_source<dim>::output_profiles_source_dat(unsigned int index){

    const unsigned int totalNodes = dof_handler.n_dofs();

    char buffer[100];

    snprintf(buffer, 100, "./output/file_%i.dat", index);

    FILE* temperature_profile;
    temperature_profile = std::fopen(buffer, "w" );

    for(unsigned  int i=0; i<totalNodes; i++)
        std::fprintf(temperature_profile, "%14.7e  %14.7e\n", r(i), D_trans[i]);

    std::fclose(temperature_profile);
}

template <int dim>
double FEM_source<dim>::time_step_corrector(double dt,double t,double time_point, unsigned int* error){

    unsigned int number_of_iterations = 0;

    while (t/yrtosec < time_point and (t+dt)/yrtosec > time_point){
        number_of_iterations += 1;
        *error -= 1;
        dt = time_steps[*error];
    }

    if (number_of_iterations>0){
        std::cout << "Time step corrector works:" << std::endl;
        std::cout << "Number of iterations = " << number_of_iterations << std::endl;
        std::cout << "Error = " << *error << std::endl;
        std::cout << " " << std::endl;
    }
    return dt;
}

template <int dim>
void FEM_source<dim>::output_cooling_curve(double time, double T_i, double T_e, unsigned int index){

    char buffer[100];

    static unsigned int open = 0;

    snprintf(buffer, 100, "./output/file_%i_cooling.dat", index);
    FILE* cooling_curve;

    if(open==0){
        cooling_curve = std::fopen(buffer, "w" );
        open ++;
    }
    else{
        cooling_curve = std::fopen(buffer, "a+" );
    }

    std::fprintf(cooling_curve, "%14.7e  %14.7e  %14.7e\n", time, T_i, T_e);
    std::cout << "T_surface = " << T_i << " K\n" << std::endl;
    std::fclose(cooling_curve);
}